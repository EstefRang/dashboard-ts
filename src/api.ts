// En tu archivo TypeScript
const apiUrl = process.env.API_URL;
if (!apiUrl) {
  throw new Error('La variable de entorno API_URL no está definida');
}

fetch(apiUrl)
  .then(response => response.json())
  .then(data => {
    console.log(data); // Aquí puedes trabajar con los datos recibidos
  })
  .catch(error => {
    console.error('Error al obtener los datos:', error);
  });
