import { defineAsyncComponent } from 'vue'
import * as vueRouter from 'vue-router'

const routes: Array<vueRouter.RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: (() => import(`../views/Dashboard.vue`))
  },
  {
    path: '/Requisiciones',
    name: 'Requisiciones',
    component: (() => import(`../views/Requisitions.vue`))
  },
  {
    path: '/EditarRequisiscion',
    name: 'EditarRequisiscion',
    component: (() => import(`../views/EditRequisitions.vue`))
  },
  {
    path: '/RevisarRequisiscion',
    name: 'RevisarRequisiscion',
    component: (() => import(`../views/ReviewRequisitions.vue`))
  },
  {
    path: '/Nuevoconcepto',
    name: 'Nuevoconcepto',
    component: (() => import(`../views/NewConcept.vue`))
  },



];

const router = vueRouter.createRouter({
  history: vueRouter.createWebHistory(import.meta.env.BASE_URL),
  routes
});

export default router;
