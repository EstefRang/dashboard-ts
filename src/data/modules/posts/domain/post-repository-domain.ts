import type { PostDomain } from "./post-domain";

export interface PostRepositoryDomain {
    list: ()=> Promise<PostDomain[]|undefined>;
}