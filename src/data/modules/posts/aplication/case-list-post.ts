import type { PostDomain } from "../domain/post-domain";
import type { PostRepositoryDomain } from "../domain/post-repository-domain";

export function useCaselistPosts(repository: PostRepositoryDomain) {
	return async (): Promise<PostDomain[] | undefined> => {
		return await repository.list();
	};
}