import axios from "axios";
import type { PostDomain } from "../domain/post-domain";
import type { PostRepositoryDomain } from "../domain/post-repository-domain";


const postList: PostDomain[] = []

const API_URL = process.env.VITE_API_URL  

export function ApiPostRepository(): PostRepositoryDomain {

    async function list(): Promise<PostDomain[] | undefined> {
        try {
            const response = await axios.get(`${API_URL}/pokemon/`);
            if (response.status === 200) {
                postList.push(...response.data.results);
                return response.data.results;
            } else {
                console.error('La solicitud HTTP falló con código:', response.status);
                return undefined;
            }
        } catch (error) {
            console.error('Ocurrió un error al realizar la solicitud HTTP:', error);
            return undefined;
        }
    }
    
    
    return {
        list,
    }
}