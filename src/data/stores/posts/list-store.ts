import { useCaselistPosts } from "@/data/modules/posts/aplication/case-list-post";
import type { PostDomain } from "@/data/modules/posts/domain/post-domain";
import { ApiPostRepository } from "@/data/modules/posts/infra/post-repository";
import { RequestStatus } from "@/data/modules/shared/RequestStatus";
import { defineStore } from "pinia";
import { computed, ref } from "vue";

export const useListPotsStore = defineStore('posts_list', () => {

  const status = ref<RequestStatus>(RequestStatus.INITIAL);
  const list_posts = ref<PostDomain[]>([]);
  const get_list_posts = computed(() => list_posts.value);
  const get_status = computed(() => status.value);
  
  const getPosts = async()=>{
      const repository = ApiPostRepository();
      status.value = RequestStatus.LOADING;
      return await useCaselistPosts(repository)()
        .then(response => {
          status.value = RequestStatus.SUCCESS;
          list_posts.value = response as  PostDomain[];
          return response;
        })
        .catch(error => {
          status.value = RequestStatus.ERROR ;
        });
  }

  return { get_status, get_list_posts, getPosts}

})