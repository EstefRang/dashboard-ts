import axios from 'axios';

const API_URL = process.env.VITE_API_URL


export async function getPokemons() {
  const response = await axios.get(`${API_URL}/pokemon/`);
  try {
    return response.data.results;
  } catch (error) {
    return []
  }
}