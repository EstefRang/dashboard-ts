import { defineComponent, ref, computed } from 'vue';
import { mdiPencil, mdiAlphaXBox } from '@mdi/js';
import { useRoute } from 'vue-router';

export default defineComponent({
    props: {
        tableData: {
            type: Array as () => Array<any>,
            required: true,
        },
        icons: {
            type: String as () => String,
            required: false,
        },
    },

    setup(props) {
        const route = useRoute();
        const itemsPerPage = ref<number>(10);
        const totalItems = ref<number>(props.tableData.length);
        const loading = ref<boolean>(false);
        const search = ref<string>('');

        const headers = computed(() => {
            return props.tableData.length > 0 && props.tableData[0] !== null
                ? Object.keys(props.tableData[0]).map((key) => ({
                    text: key.charAt(0).toUpperCase() + key.slice(1),
                    align: 'start',
                    value: key,
                })).concat({
                    text: 'Actions',
                    align: 'start',
                    sortable: false,
                    value: 'actions',
                })
                : [];
        });

        const loadItems = (options: any) => {
            const { pagination, search } = options;
            loading.value = true;
            setTimeout(() => {
                totalItems.value = props.tableData.length ?? 0;
                loading.value = false;
            }, 1000);
        };

        const editItem = (item: any) => {
            console.log('Editing item:', item);
        };

        const deleteItem = (item: any) => {
            console.log('Deleting item:', item);
        };

        return {
            itemsPerPage,
            headers,
            totalItems,
            loading,
            search,
            loadItems,
            editItem,
            deleteItem,
            mdiAlphaXBox,
        };
    },
});
