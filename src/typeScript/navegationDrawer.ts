import { ref } from 'vue';
import { mdiChevronDown, mdiAccountCircle } from '@mdi/js';

export interface ListItem {
  icon: string;
  title: string;
  path: string;
  active: boolean;
}

export const icons = ref({
  mdiChevronDown,
  mdiAccountCircle,
});

export default function useHomeItems() {
  const homeItems = ref<ListItem[]>([
    { title: "CATALOGO", value: "CATALOGO", path: "/", active: false },
    { title: "REQUISICIONES", value: "REQUISICIONES", path: "/Requisiciones", active: false },
    { title: "EDITAR REQUISICION", value: "EDITAR REQUISICION", path: "/EditarRequisiscion", active: false },
  ]);

  const setActiveItem = (item: ListItem) => {
    homeItems.value.forEach((i: ListItem) => {
      i.active = (i === item);
    });
  };

  return {
    homeItems,
    icons,
    setActiveItem,
  };
}
