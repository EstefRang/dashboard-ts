import { ref, computed } from 'vue';
import { mdiPencil, mdiDeleteOutline, mdiListStatus, mdiFileChartOutline, mdiFaceManProfile, mdiPrinter, mdiContentCopy } from '@mdi/js';
import { useRoute } from 'vue-router';

 export interface Dessert {
  name: string;
  calories: number;
  fat: number;
  carbs: number;
  protein: number;
  status: string;
}

export default function useDataTable() {
  const itemsPerPageOptions = [5, 10, 15, 20];
  const itemsPerPage = ref(itemsPerPageOptions[0]);
  const dialog = ref(false);
  const dialogDelete = ref(false);
  const editedIndex = ref(-1);
  const editedItem = ref<Dessert>({
    name: '',
    calories: 0,
    fat: 0,
    carbs: 0,
    protein: 0,
    status: ''
  });

  const defaultItem: Dessert = {
    name: '',
    calories: 0,
    fat: 0,
    carbs: 0,
    protein: 0,
    status: ''
  };

  const mdiIcons = {
    mdiPencil,
    mdiDeleteOutline,
    mdiListStatus,
    mdiFaceManProfile,
    mdiPrinter,
    mdiFileChartOutline,
    mdiContentCopy
  };

  const currentRoute = useRoute();

  const getColors = (status: string) => {
    switch (status.toLowerCase()) {
      case 'pendiente':
      case 'pending':
        return 'amber';
      case 'aceptada':
        return 'green';
      case 'rechazada':
        return 'red';
      default:
        return 'deep-purple';
    }
  };

  const showActionIcons = (item: Dessert, action: string) => {
    if (currentRoute.path === '/') {
      return action === 'delete' || action === 'edit';
    } else if (currentRoute.path === '/Requisitions') {
      return action !== 'delete';
    } else {
      return true;
    }
  };

  const desserts = ref<Dessert[]>([
    { name: 'Bulbasaur', calories: 120, fat: 2.0, carbs: 18, protein: 8.0, status: 'PENDIENTE' },
    { name: 'Charmander', calories: 180, fat: 4.0, carbs: 25, protein: 10.0, status: 'ACEPTADA' },
    { name: 'Squirtle', calories: 150, fat: 3.0, carbs: 20, protein: 9.0, status: 'PENDIENTE' },
    { name: 'Pikachu', calories: 110, fat: 1.5, carbs: 15, protein: 6.0, status: 'ACEPTADA' },
    { name: 'Meowth', calories: 130, fat: 2.5, carbs: 18, protein: 7.0, status: 'PENDIENTE' },
    { name: 'Psyduck', calories: 160, fat: 3.5, carbs: 22, protein: 8.5, status: 'ACEPTADA' },
    { name: 'Snorlax', calories: 400, fat: 15.0, carbs: 50, protein: 20.0, status: 'PENDIENTE' },
    { name: 'Articuno', calories: 250, fat: 8.0, carbs: 32, protein: 12.0, status: 'RECHAZADA' }
  ]);

  const close = () => {
    dialog.value = false;
    editedItem.value = { ...defaultItem };
    editedIndex.value = -1;
  };

  const closeDelete = () => {
    dialogDelete.value = false;
    editedItem.value = { ...defaultItem };
    editedIndex.value = -1;
  };

  const save = () => {
    if (editedIndex.value > -1) {
      Object.assign(desserts.value[editedIndex.value], editedItem.value);
    } else {
      desserts.value.push({ ...editedItem.value });
    }
    close();
  };

  const editItem = (item: Dessert) => {
    editedIndex.value = desserts.value.indexOf(item);
    editedItem.value = { ...item };
    dialog.value = true;
  };

  const deleteItem = (item: Dessert) => {
    editedIndex.value = desserts.value.indexOf(item);
    editedItem.value = { ...item };
    dialogDelete.value = true;
  };

  const deleteItemConfirm = () => {
    desserts.value.splice(editedIndex.value, 1);
    closeDelete();
  };

  return {
    dialog,
    itemsPerPage,
    itemsPerPageOptions,
    dialogDelete,
    editedIndex,
    editedItem,
    mdiIcons,
    currentRoute,
    desserts,
    getColors,
    showActionIcons,
    close,
    closeDelete,
    save,
    editItem,
    deleteItem,
    deleteItemConfirm
  };
}
