import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

import dotenv from 'dotenv';
dotenv.config();

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    
  ],
  define: {
    // Hace que las variables de entorno estén disponibles en la aplicación Vue
    'process.env': JSON.stringify(process.env),
    // Agrega otras variables de entorno aquí si es necesario
  },
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  }
})
